<?php

namespace App\Http\Controllers;

use App\Twotwob;
use Illuminate\Http\Request;

class TwotwobController extends Controller
{
    public function store(){
        $objTwotwob=new Twotwob();
        $objTwotwob->sub_title=$_POST['sub_title'];
        $objTwotwob->teacher_name=$_POST['teacher_name'];
        $objTwotwob->day=$_POST['day'];
        $objTwotwob->time=$_POST['time'];
        $status=$objTwotwob->save();
        return redirect()->route('Twotwobroutine');
    }
    public function index(){
        $objTwotwob=new Twotwob();
        $allData=$objTwotwob->paginate(20);
        return view("Twotwob/index",compact('allData'));
    }
    public function index1(){
        $objTwotwob=new Twotwob();
        $allData=$objTwotwob->paginate(20);
        return view("Twotwob/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objTwotwob=new Twotwob();
        $oneData=$objTwotwob->find($id);
        return view("Twotwob/edit",compact('oneData'));
    }

    public function update(){
        $objTwotwob=new Twotwob();
        $oneData=$objTwotwob->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Twotwobroutine');
    }
    public function delete($id){
        $objTwotwob=new Twotwob();
        $oneData=$objTwotwob->find($id)->delete();
        return redirect()->route('Twotwobroutine');
    }
}
