<?php

namespace App\Http\Controllers;

use App\FourtwoA;
use Illuminate\Http\Request;

class FourtwoAcontroller extends Controller
{
    public function store(){
        $objFourtwoA=new FourtwoA();
        $objFourtwoA->sub_title=$_POST['sub_title'];
        $objFourtwoA->teacher_name=$_POST['teacher_name'];
        $objFourtwoA->day=$_POST['day'];
        $objFourtwoA->time=$_POST['time'];
        $status=$objFourtwoA->save();
        return redirect()->route('FourtwoAroutine');
    }
    public function index(){
        $objFourtwoA=new FourtwoA();
        $allData=$objFourtwoA->paginate(20);
        return view("FourtwoA/index",compact('allData'));
    }
    public function index1(){
        $objFourtwoA=new FourtwoA();
        $allData=$objFourtwoA->paginate(20);
        return view("FourtwoA/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objFourtwoA=new FourtwoA();
        $oneData=$objFourtwoA->find($id);
        return view("FourtwoA/edit",compact('oneData'));
    }

    public function update(){
        $objFourtwoA=new FourtwoA();
        $oneData=$objFourtwoA->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('FourtwoAroutine');
    }

    public function delete($id){
        $objFourtwoA=new FourtwoA();
        $oneData=$objFourtwoA->find($id)->delete();
        return redirect()->route('FourtwoAroutine');
    }

}
