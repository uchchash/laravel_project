<?php

namespace App\Http\Controllers;

use App\Oneoneb;
use Illuminate\Http\Request;

class OneonebController extends Controller
{
    public function store(){
        $objOneoneb=new Oneoneb();
        $objOneoneb->sub_title=$_POST['sub_title'];
        $objOneoneb->teacher_name=$_POST['teacher_name'];
        $objOneoneb->day=$_POST['day'];
        $objOneoneb->time=$_POST['time'];
        $status=$objOneoneb->save();
        return redirect()->route('Oneonebroutine');
    }
    public function index(){
        $objOneoneb=new Oneoneb();
        $allData=$objOneoneb->paginate(20);
        return view("Oneoneb/index",compact('allData'));
    }
    public function index1(){
        $objOneoneb=new Oneoneb();
        $allData=$objOneoneb->paginate(20);
        return view("Oneoneb/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objOneoneb=new Oneoneb();
        $oneData=$objOneoneb->find($id);
        return view("Oneoneb/edit",compact('oneData'));
    }

    public function update(){
        $objOneoneb=new Oneoneb();
        $oneData=$objOneoneb->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Oneonebroutine');
    }
    public function delete($id){
        $objOneoneb=new Oneoneb();
        $oneData=$objOneoneb->find($id)->delete();
        return redirect()->route('Oneonebroutine');
    }
}
