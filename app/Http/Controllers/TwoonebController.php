<?php

namespace App\Http\Controllers;

use App\Twooneb;
use Illuminate\Http\Request;

class TwoonebController extends Controller
{
    public function store(){
        $objTwooneb=new Twooneb();
        $objTwooneb->sub_title=$_POST['sub_title'];
        $objTwooneb->teacher_name=$_POST['teacher_name'];
        $objTwooneb->day=$_POST['day'];
        $objTwooneb->time=$_POST['time'];
        $status=$objTwooneb->save();
        return redirect()->route('Twoonebroutine');
    }
    public function index(){
        $objTwooneb=new Twooneb();
        $allData=$objTwooneb->paginate(20);
        return view("Twooneb/index",compact('allData'));
    }
    public function index1(){
        $objTwooneb=new Twooneb();
        $allData=$objTwooneb->paginate(20);
        return view("Twooneb/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objTwooneb=new Twooneb();
        $oneData=$objTwooneb->find($id);
        return view("Twooneb/edit",compact('oneData'));
    }

    public function update(){
        $objTwooneb=new Twooneb();
        $oneData=$objTwooneb->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Twoonebroutine');
    }
    public function delete($id){
        $objTwooneb=new Twooneb();
        $oneData=$objTwooneb->find($id)->delete();
        return redirect()->route('Twoonebroutine');
    }
}
